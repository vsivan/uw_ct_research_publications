<?php
/**
 * @file
 * uw_ct_research_publications.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function uw_ct_research_publications_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'research_topics_publications';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Research Topics Publications';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Related Research Topics';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Biblio: Biblio Citation */
  $handler->display->display_options['fields']['citation']['id'] = 'citation';
  $handler->display->display_options['fields']['citation']['table'] = 'biblio';
  $handler->display->display_options['fields']['citation']['field'] = 'citation';
  $handler->display->display_options['fields']['citation']['label'] = '';
  $handler->display->display_options['fields']['citation']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Sort criterion: Biblio: Year of Publication */
  $handler->display->display_options['sorts']['biblio_year']['id'] = 'biblio_year';
  $handler->display->display_options['sorts']['biblio_year']['table'] = 'biblio';
  $handler->display->display_options['sorts']['biblio_year']['field'] = 'biblio_year';
  $handler->display->display_options['sorts']['biblio_year']['order'] = 'DESC';
  /* Contextual filter: Content: Research Topics (field_related_research_topics) */
  $handler->display->display_options['arguments']['field_related_research_topics_target_id']['id'] = 'field_related_research_topics_target_id';
  $handler->display->display_options['arguments']['field_related_research_topics_target_id']['table'] = 'field_data_field_related_research_topics';
  $handler->display->display_options['arguments']['field_related_research_topics_target_id']['field'] = 'field_related_research_topics_target_id';
  $handler->display->display_options['arguments']['field_related_research_topics_target_id']['default_action'] = 'default';
  $handler->display->display_options['arguments']['field_related_research_topics_target_id']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['field_related_research_topics_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_related_research_topics_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_related_research_topics_target_id']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'biblio' => 'biblio',
  );

  /* Display: Related Research Topics */
  $handler = $view->new_display('entity_view', 'Related Research Topics', 'entity_view_1');
  $handler->display->display_options['entity_type'] = 'node';
  $handler->display->display_options['bundles'] = array(
    0 => 'research_demos',
    1 => 'research_topics',
  );
  $translatables['research_topics_publications'] = array(
    t('Master'),
    t('Related Research Topics'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('All'),
  );
  $export['research_topics_publications'] = $view;

  return $export;
}
